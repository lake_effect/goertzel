# goertzel

![cargo badge](https://img.shields.io/crates/v/goertzel-filter.svg)

Rust implementation of
the [Goertzel filter](https://en.wikipedia.org/wiki/Goertzel_algorithm).

The Goertzel filter is faster than the FFT for a small range of frequencies of
interest.

## interface

- `pub fn filter_naive(input: &Vec<f64>, linear_freq: f64) -> Vec<Complex<f64>>`
    
    Just applies the filter equations and gives you the complete output.

- `pub fn dft(input: &Vec<f64>, linear_freq: f64) -> Complex<f64>`
    
    Computes the associated (closest) DFT term to a given linear frequency.

- `pub fn dft_power(input: &Vec<f64>, linear_freq: f64) -> f64`
    
    Computes the power of the signal at a given linear frequency.

## TODO

- Docs
