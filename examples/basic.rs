extern crate goertzel_filter;
extern crate itertools_num;

use itertools_num::linspace;

fn main() {
  // 440Hz as wave frequency (middle A)
  let freq: f64 = 440.0;
  // Time vector sampled at 880 times/s (~Nyquist), over 1s
  let delta: f64 = 1.0 / freq / 2.0;
  let time_1s = linspace(0.0, 1.0, (freq / 2.0) as usize)
    .map(|sample| { sample * delta});

  let sine_440: Vec<f64> = time_1s.map(|time_sample| {
    (time_sample * freq).sin()
  }).collect();

  // println!("Sine wave looks like: {:?}", &sine_440);

  let filter_output = goertzel_filter::filter_naive(&sine_440, 440.0);

  println!("Filtered output: ");
  println!("{:?}", filter_output);
}
