extern crate goertzel_filter;
extern crate itertools_num;

use itertools_num::linspace;

fn main() {
  // Maximum frequency (so sample at double this)
  let max_freq: f64 = 500.0;
  let interval_length = 0.5;
  
  // Time vector sampled at 1000 times/s (~Nyquist), over 500ms
  let delta: f64 = &interval_length / &max_freq / 2.0;
  let time_500ms = linspace(0.0, interval_length, (&max_freq / 2.0) as usize)
    .map(|sample| { sample * delta });

  // Unit impulse at time zero
  let mut impulse_500ms: Vec<f64> = time_500ms.collect();
  impulse_500ms[0] = 1.0;

  let dft_bins: Vec<f64> = vec![1.0, 10.0, 100.0, 250.0, 500.0];
  
  let impulse_dfts = dft_bins.iter().map(|&dft_freq| {
    goertzel_filter::dft_power(&impulse_500ms, dft_freq)
  });

  println!("");
  for (ind, pow) in impulse_dfts.enumerate() {
    // These should be roughly equal at every bin, since we input a unit
    // impulse.
    println!("Power at {}Hz: {}", dft_bins[ind], pow);
  }
}
