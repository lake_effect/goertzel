extern crate goertzel_filter;
extern crate itertools_num;

use itertools_num::linspace;

fn main() {
  // 440Hz as wave frequency (middle A)
  let freq: f64 = 440.0;
  let interval_length = 1.0;
  // Time vector sampled at 880 times/s (~Nyquist), over 1s
  let delta: f64 = &interval_length / &freq / 2.0;
  let time_1s = linspace(0.0, interval_length, (&freq / 2.0) as usize)
    .map(|sample| { sample * delta });

  
  let (sine_440,
       sine_100): (Vec<f64>, Vec<f64>) =
    time_1s.map(|time_sample| {
      let sample_440 = (freq * &time_sample).sin();
      let sample_100 = (100.0 * &time_sample).sin();
      
      (sample_440, sample_100)
    }).unzip();

  let summed_signal: Vec<f64> = sine_440
    .iter()
    .zip(sine_100.iter())
    .map(|(sample_440, sample_100)| {
      sample_440 + sample_100
    }).collect();
    
  let power_440 = goertzel_filter::dft_power(&sine_440, 440.0);  
  let power_100 = goertzel_filter::dft_power(&sine_100, 440.0);
  
  let summed_signal_power = goertzel_filter::dft_power(&summed_signal, 440.0);
  
  let summed_power = (power_440.sqrt() + power_100.sqrt()).powi(2);

  println!("");
  println!("Summed power: {}", &summed_power);
  println!("Summed signal power: {}", &summed_signal_power);
  println!("Difference: {}", &summed_power - &summed_signal_power);
}
