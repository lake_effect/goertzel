extern crate goertzel_filter;
extern crate itertools_num;

use itertools_num::linspace;

fn main() {
  // 440Hz as wave frequency (middle A)
  let freq: f64 = 440.0;
  // Time vector sampled at 880 times/s (~Nyquist), over 1s
  let delta: f64 = 1.0 / freq / 2.0;
  let time_1s = linspace(0.0, 1.0, (freq / 2.0) as usize).map(|sample| { sample * delta});
  let sine_440: Vec<f64> = time_1s.map(|time_sample| {
    (time_sample * freq).sin()
  }).collect();

  let dft_output = goertzel_filter::dft(&sine_440, 440.0);

  println!("DFT output at 440Hz: ");
  println!("{:?}", dft_output);
  println!("");

  let dft_power = goertzel_filter::dft_power(&sine_440, 440.0);
  println!("DFT power at 440Hz: ");
  println!("{:?}", &dft_power);
  println!("");
  
  let dft_power_off = goertzel_filter::dft_power(&sine_440, 10.0);
  println!("DFT power at 10Hz: ");
  println!("{:?}", &dft_power_off);
  println!("");
  
  println!("Power ratio: {}", dft_power / dft_power_off);
}
